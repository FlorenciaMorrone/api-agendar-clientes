<?php

namespace App\Endpoints;

use App\Core\Abstracts\AbstractEndpoints;
use Symfony\Component\HttpFoundation\JsonResponse;

class Pruebas extends AbstractEndpoints
{
    use \App\Core\Validators\MakeErrorTrait;

    public function prueba()
    {
        return new JsonResponse(['_data' => [
            'message' => 'Hola mundo'
        ]], 202);
    }
}
