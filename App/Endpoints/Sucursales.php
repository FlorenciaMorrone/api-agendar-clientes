<?php

namespace App\Endpoints;

use App\Database\Repos\Cliente;
use App\Core\Abstracts\AbstractEndpoints;
use App\Database\Repos\Direccion;
use App\Database\Repos\Sucursal;
use Symfony\Component\HttpFoundation\JsonResponse;

class Sucursales extends AbstractEndpoints
{
    public function asignarNuevaSucursal()
    {
        $cliente = Cliente::getCliente($this->getParam('id'));
        $direccion = Direccion::crearDireccion($this->getData());
        $sucursal = Sucursal::crearSucursal($this->getData);
        $sucursal = Direccion::asignarDireccionASucursal($sucursal['id'], $direccion['id']);
        $cliente = Cliente::asignarSucursalACliente($this['id'], $sucursal['id']);

        if($cliente['sucursal_id'])
        {
            return new JsonResponse([   
                '_data' => [
                    'message' => 'Sucursal asignada correctamente',
                    'info' => [
                        'numero de sucursal' => $sucursal['numero_sucursal'],
                        'nombre de la empresa' => $cliente['nombre_empresa']
                    ]
                ]
            ], 200);
        }
    }

    public function modificar()
    {

        $sucursal= Sucursal::editarSucursal($this->getData());
        $direccion = Direccion::getDireccion($sucursal['direccion_id']);
        $direccion = Direccion::modificarDireccion($this->getData(),$direccion['id']);

        return new JsonResponse([
            '_data' => [
                'message' => 'Sucursal modificada exitosamente:',
                'info' => [
                    'numero de sucursal' => $sucursal['numero_sucursal'],
                    'pais'=> $direccion['pais'],
                    'estado'=> $direccion['estado'],
                    'ciudad'=> $direccion['ciudad'],
                    'codigo_postal'=> $direccion['codigo_postal'],
                    'calle'=> $direccion['calle'],
                    'numero_principal'=> $direccion['numero_principal'],
                    'numero_secundario'=> $direccion['numero_secundario'],
                    'numero_local'=> $direccion['numero_local'],
                ]
            ]
        ], 200);
    }

    public function getSucursal()
    {
        $sucursal= Sucursal::getSucursal($this->getParam('id'));

        return new JsonResponse([
            '_data' => [
                'message' => 'Sucursal:',
                'info' => [
                    'numero de sucursal' => $sucursal['numero_sucursal']
                ]
            ]
        ], 200);
    }

    public function getSucursales()
    {
        $clienteid = Cliente::getCliente($this->getParam('id_cliente'));
        $sucursales= Sucursal::getSucursalesPorCliente($clienteid);

        return new JsonResponse([
            '_data' => ['_data'=> $sucursales]], 200);
    }

    public function eliminar()
    {
        $sucursal = Sucursal::getSucursal($this->getParam('id'));
        $direccion = Direccion::getDireccion($sucursal['direccion_id']);
        $direccion = Direccion::eliminarDireccion($direccion['id']);
        $sucursal = Sucursal::eliminarSucursal($sucursal['id']);

        if($direccion){
            
            if($sucursal)
            {
                return new JsonResponse([   
                    '_data' => [
                        'message' => 'sucursal eliminada correctamente'
                    ]
                ], 200);
            }    
            else
            {
                return new JsonResponse([   
                    '_data' => [
                        'message' => 'Error al eliminar sucursal'
                    ]
                ], 200);
            }
        }
        else
        {
            return new JsonResponse([   
                '_data' => [
                    'message' => 'Error al eliminar direccion'
                ]
            ], 200);
        }
        
    }

}