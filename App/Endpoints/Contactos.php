<?php

namespace App\Endpoints;

use App\Database\Repos\Cliente;
use App\Database\Repos\Contacto;

use App\Database\Repos\Direccion;
use App\Core\Abstracts\AbstractEndpoints;
use Symfony\Component\HttpFoundation\JsonResponse;

class Contactos extends AbstractEndpoints
{
    public function asignarNuevoContacto()
    {
        $cliente = Cliente::getCliente($this->getParam('id'));
        $direccion = Direccion::crearDireccion($this->getData());
        $contacto = Contacto::crearContacto($this->getData());
        $contacto = Direccion::asignarDireccionAContacto($contacto['id'],$direccion['id']);
        $contacto = Contacto::asignarContactoACliente($cliente['id'], $contacto['id']);

        return new JsonResponse([   
            '_data' => [
                'message' => 'contacto creado correctamente',
                'info' => [
                    'nombre' => $contacto['nombre'],
                    'apellido' => $contacto['apellido'],
                    'mail' => $contacto['mail'],
                ]
            ]
        ], 200);
    }

    public function modificar()
    {
        $contacto = Contacto::getContacto($this->getParam(['id']));
        $direccion = Direccion::getDireccion($contacto['direccion_id']);
        $contacto = Contacto::modificarContacto($this->getData());
        $direccion = Direccion::modificarDireccion($this->getData(), $direccion['id']);

        return new JsonResponse([
            '_data' => [
                'message' => 'contacto modificado exitosamente:',
                'info' => [
                    'nombre' => $contacto['nombre'],
                    'apellido' => $contacto['apellido'],
                    'mail' => $contacto['mail'],
                    'pais'=> $direccion['pais'],
                    'estado'=> $direccion['estado'],
                    'ciudad'=> $direccion['ciudad'],
                    'codigo_postal'=> $direccion['codigo_postal'],
                    'calle'=> $direccion['calle'],
                    'numero_principal'=> $direccion['numero_principal'],
                    'numero_secundario'=> $direccion['numero_secundario'],
                    'numero_local'=> $direccion['numero_local'],
                ]
            ]
        ], 200);
    }

    public function getContactos()
    {
        $clienteId = $this->getParam('id_cliente');
        $contactos= Contacto::getContactos($clienteId);

        return new JsonResponse(['_data'=> $contactos], 200);
    }

    public function getContactoPorNombre()
    {
        $contacto= Contacto::getContactoPorNombre($this->getParam('nombre'));

        return new JsonResponse([
            '_data' => [
                'message' => 'Empresa:',
                'info' => [
                    'nombre' => $contacto['nombre'],
                    'apellido' => $contacto['apellido'],
                    'mail' => $contacto['mail'],
                ]
            ]
        ], 200);
    }

    public function eliminar()
    {
        $contacto = Contacto::getContacto($this->getParam('id'));
        $direccion = Direccion::getDireccion($contacto['direccion_id']);
        $direccion = Direccion::eliminarDireccion($direccion['id']);
        $contacto = contacto::eliminarContacto($contacto['id']);

        if($direccion){
            
            if($contacto)
            {
                return new JsonResponse([   
                    '_data' => [
                        'message' => 'contacto eliminada correctamente'
                    ]
                ], 200);
            }    
            else
            {
                return new JsonResponse([   
                    '_data' => [
                        'message' => 'Error al eliminar contacto'
                    ]
                ], 200);
            }
        }
        else
        {
            return new JsonResponse([   
                '_data' => [
                    'message' => 'Error al eliminar direccion'
                ]
            ], 200);
        }
        
    }
}