<?php

namespace App\Endpoints;

use App\Database\Repos\Cliente;
use App\Core\Abstracts\AbstractEndpoints;

use Symfony\Component\HttpFoundation\JsonResponse;

class Clientes extends AbstractEndpoints
{
    use \App\Core\Validators\MakeErrorTrait;

    public function crear()
    {
        $cliente = Cliente::crearCliente($this->getData());

        return new JsonResponse([   
            '_data' => [
                'message' => 'Cliente creado correctamente',
                'info' => [
                    'nombre de la empresa' => $cliente['nombre_empresa']
                ]
            ]
        ], 200);
    }

    public function modificar()
    {
        $cliente = Cliente::modificarCliente($this->getData(), $this->getParam('id'));

        return new JsonResponse([
            '_data' => [
                'message' => 'cliente modificado exitosamente:',
                'info' => [
                    'nombre de la empresa' => $cliente['nombre_empresa'],
                    'numero de fax' => $cliente['numero_fax']
                ]
            ]
        ], 200);
    }

    public function getClientes()
    {
        $usuarioId = $this->getParam('id');
        $clientes= Cliente::getClientes($usuarioId);

        return new JsonResponse(['_data'=> $clientes], 200);
    }

    public function getClientePorNombre()
    {
        $cliente= Cliente::getClientePorNombre($this->getParam('nombre_empresa'));

        return new JsonResponse([
            '_data' => [
                'message' => 'Empresa:',
                'info' => [
                    'nombre' => $cliente['nombre_empresa']
                ]
            ]
        ], 200);
    }

    public function eliminar()
    {
        $cliente = Cliente::eliminarCliente($this->getParam('id'));

        if($cliente)
        {
            return new JsonResponse([   
                '_data' => [
                    'message' => 'Cliente eliminado correctamente correctamente'
                ]
            ], 200);
        }    
        else
        {
            return new JsonResponse([   
                '_data' => [
                    'message' => 'Error al eliminar cliente'
                ]
            ], 200);
            
        }
    }



}