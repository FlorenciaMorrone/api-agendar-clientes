<?php

namespace App\Endpoints;

use App\Database\Repos\Usuario;
use App\Core\Abstracts\AbstractEndpoints;
use App\Database\Repos\Rol;
use Symfony\Component\HttpFoundation\JsonResponse;

class Usuarios extends AbstractEndpoints
{
    use \App\Core\Validators\MakeErrorTrait;
    
    public function crear()
    {
        $usuario = Usuario::crearUsuario($this->getData());
        $usuario = Rol::asignarRol($this->getParam('rol_id'), $usuario['id']);

        return new JsonResponse([
            '_data' => [
                'message' => 'Usuario creado correctamente',
                'info' => [
                    'nombre' => $usuario['nombre'],
                    'apellido' => $usuario['apellido'],
                    'mail' => $usuario['mail']
                ]
            ]
        ], 200);
    }

    public function getUsuario()
    {
        $usuario= Usuario::getUsuario($this->getParam('id'));

        return new JsonResponse([
            '_data' => [
                'message' => 'Usuario:',
                'info' => [
                    'nombre' => $usuario['nombre'],
                    'apellido' => $usuario['apellido'],
                    'mail' => $usuario['mail']
                ]
            ]
        ], 200);
    }

    public function modificar()
    {
        $usuario = Usuario::modificarUsuario($this->getData(), $this->getParam('id'));

        return new JsonResponse([
            '_data' => [
                'message' => 'Usuario modificado exitosamente:',
                'info' => [
                    'nombre' => $usuario['nombre'],
                    'apellido' => $usuario['apellido']
                ]
            ]
        ], 200);
    }


    public function pruebas()
    {
        return new JsonResponse([
            '_data' => [
                'message' => 'Hola mundo'
            ]
        ], 200);
    }
}
