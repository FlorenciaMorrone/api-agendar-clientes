<?php namespace App\Database\Repos;

use PDO;
use App\Database\Repo;
use App\Database\Conexion;


class Sucursal extends Repo
{

    /**
     * funcion crear sucursal
     *
     * @param Array $data
     * @return Sucursal
     */
    public static function crearSucursal($data)
    {
        
        $sql = "INSERT INTO sucursal (
            numero_sucursal, 
            cliente_id,
            numero_fax,
            direccion_id) 
            VALUES (
                :numero_sucursal, 
                :cliente_id,
                :numero_fax,
                :direccion_id)";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue('numero_sucursal', $data['numero_sucursal'], PDO::PARAM_STR);      
        $sentencia->bindValue('cliente_id', $data['cliente_id'], PDO::PARAM_STR); 
        $sentencia->bindValue('numero_fax', $data['numero_fax'], PDO::PARAM_STR);   
        $sentencia->bindValue('direccion_id', $data['direccion_id'], PDO::PARAM_STR);        
        $sentencia->execute(); 
        $lastId = $conexion->lastInsertId();  
        $sentencia = $conexion->prepare("SELECT * FROM cliente WHERE id = :id");     
        $sentencia->bindValue(':id', $lastId, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();

        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * funcion modificar sucursal
     *
     * @param Array $data
     * @return Sucursal
     */
    public static function editarSucursal($data)
    {
        
        $sql = "UPDATE sucursal 
                SET numero_sucursal = :numero_sucursal,
                    numero_fax = :numero_fax
                WHERE id = :id";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue('numero_sucursal', $data['numero_sucursal'], PDO::PARAM_STR);      
        $sentencia->bindValue('numero_fax', $data['numero_fax'], PDO::PARAM_STR); 
        $sentencia->bindValue('id', $data['id'], PDO::PARAM_STR);         
        $sentencia->execute(); 
        $lastId = $conexion->lastInsertId();  
        $sentencia = $conexion->prepare("SELECT * FROM sucursal WHERE id = :id");     
        $sentencia->bindValue(':id', $lastId, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();

        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * funcion para eliminar un sucursal
     *
     * @param int $idSucursal
     * @return bool
     */
    public static function eliminarSucursal($idsucursal)
    {
        $sql = "DELETE FROM sucursal WHERE id=:id";
        
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue('id', $idsucursal, PDO::PARAM_STR);    
        $sentencia->execute(); 
        Conexion::closeConexion();
        return true;
    }

    /**
     * funcion para obtener una lista de sucursales por ide del cliente
     *
     * @param int $idCliente
     */
    public static function getSucursalesPorCliente($idCliente)
    {
        $sql = "SELECT * FROM sucursal WHERE cliente_id = :id";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $idCliente, PDO::PARAM_STR);
        $sentencia->execute();        
        Conexion::closeConexion();
        return $sentencia->fetchAll(PDO::FETCH_ASSOC);
    }   

    /**
     * funcion para obtener scursal
     *
     * @param int $idSucursal
     * @return Sucursal
     */
    public static function getSucursal($idSucursal)
    {
        $sql = "SELECT * FROM sucursal WHERE id = :id";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $idSucursal, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }
}