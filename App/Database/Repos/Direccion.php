<?php namespace App\Database\Repos;

use PDO;
use App\Database\Repo;
use App\Database\Conexion;

class Direccion extends Repo
{
    /**
     * funcion para crear direccion
     *
     * @param Array $data
     * @return Direccion
     */
    public static function crearDireccion($data)
    {
        $sql = "INSERT INTO direccion (
                                        pais,
                                        estado,
                                        ciudad,
                                        codigo_postal,
                                        calle,
                                        numero_principal,
                                        numero_secundario,
                                        numero_local) 
                                        VALUES (
                                            :pais,
                                            :estado,
                                            :ciudad,
                                            :codigo_postal,
                                            :calle,
                                            :numero_principal,
                                            :numero_secundario,
                                            :numero_local)";
        
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue('pais', $data['pais'], PDO::PARAM_STR);  
        $sentencia->bindValue('estado', $data['estado'], PDO::PARAM_STR); 
        $sentencia->bindValue('ciudad', $data['ciudad'], PDO::PARAM_STR); 
        $sentencia->bindValue('codigo_postal', $data['codigo_postal'], PDO::PARAM_STR); 
        $sentencia->bindValue('calle', $data['calle'], PDO::PARAM_STR);       
        $sentencia->bindValue('numero_principal', $data['numero_principal'], PDO::PARAM_STR);
        $sentencia->bindValue('numero_secundario', $data['numero_secundario'], PDO::PARAM_STR); 
        $sentencia->bindValue('numero_local', $data['nombre_empresa'], PDO::PARAM_STR);        
        $sentencia->execute(); 
        $lastId = $conexion->lastInsertId();  
        $sentencia = $conexion->prepare("SELECT * FROM cliente WHERE id = :id");     
        $sentencia->bindValue(':id', $lastId, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();

        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * funcion para asignar direccion a un contacto
     *
     * @param int $idContacto
     * @param int $idDireccion
     * @return bool
     */
    public static function asignarDireccionAContacto($idContacto,$idDireccion){

        $sql = "UPDATE contacto SET direccion_id = :direccion_id WHERE id = :id";
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':direccion_id', $idDireccion, PDO::PARAM_STR);
        $sentencia->bindValue(':id', $idContacto, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return true;
    }

    /**
     * funcion para asignar direccion a una sucursal
     *
     * @param int $idSucursal
     * @param int $idDireccion
     * @return bool
     */
    public static function asignarDireccionASucursal($idSucursal,$idDireccion){

        $sql = "UPDATE sucursal SET direccion_id = :direccion_id WHERE id = :id";
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':direccion_id', $idDireccion, PDO::PARAM_STR);
        $sentencia->bindValue(':id', $idSucursal, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return true;
    }

    /**
     * funcion para modificar una direccion
     *
     * @param Array $data
     * @param int $idDireccion
     * @return Direccion
     */
    public static function modificarDireccion($data, $idDireccion)
    {
        $sql = "UPDATE direccion 
                SET pais = :pais, 
                estado = :estado,
                ciudad = :ciudad, 
                codigo_postal = :codigo_postal, 
                calle = :calle, 
                numero_principal = :numero_principal,
                numero_secundario = :numero_secundario,
                numero_local = :numero_local
                WHERE id = :id";        
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $idDireccion);
        $sentencia->bindValue(':pais', $data['pais']);
        $sentencia->bindValue(':estado', $data['estado']);
        $sentencia->bindValue(':ciudad', $data['ciudad']);
        $sentencia->bindValue(':codigo_postal', $data['codigo_postal']);
        $sentencia->bindValue(':calle', $data['calle']);
        $sentencia->bindValue(':numero_principal', $data['numero_principal']);
        $sentencia->bindValue(':numero_secundario', $data['numero_secundario']);
        $sentencia->bindValue(':numero_local', $data['numero_local']);
        $sentencia->execute();
        $sentencia = $conexion->prepare("SELECT * FROM direccion WHERE id = :id");     
        $sentencia->bindValue(':id', $idDireccion, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();

        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * funcion para eliminar una direccion
     *
     * @param int $idDireccion
     * @return bool
     */
    public static function eliminarDireccion($idDireccion)
    {
        $sql = "DELETE FROM direccion WHERE id=:id";
        
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue('id', $idDireccion, PDO::PARAM_STR);    
        $sentencia->execute(); 
        Conexion::closeConexion();
        return true;
    }

    /**
     * funcion para obtener una direccion
     *
     * @param int $idDireccion
     * @return bool
     */
    public static function getDireccion($idDireccion)
    {
        $sql = "SELECT * FROM direccion WHERE id = :id";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $idDireccion, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }


}