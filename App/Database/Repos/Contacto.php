<?php namespace App\Database\Repos;

use PDO;
use App\Database\Repo;
use App\Database\Conexion;


class Contacto extends Repo
{
    /**
     * funcion para crear contacto
     *
     * @param array $data
     * @return Contacto
     */
    public static function crearContacto($data)
    {
        $sql = "INSERT INTO contacto (
            nombre,
            apellido,
            mail,
            telefono,
            cliente_id) 
            VALUES (
                :nombre,
                :apellido,
                :mail,
                :telefono,
                :cliente_id)";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue('nombre', $data['nombre'], PDO::PARAM_STR);  
        $sentencia->bindValue('apellido', $data['apellido'], PDO::PARAM_STR); 
        $sentencia->bindValue('mail', $data['mail'], PDO::PARAM_STR); 
        $sentencia->bindValue('teledono', $data['teledono'], PDO::PARAM_STR); 
        $sentencia->bindValue('cliente_id', $data['cliente_id'], PDO::PARAM_STR);          
        $sentencia->execute(); 
        $lastId = $conexion->lastInsertId();  
        $sentencia = $conexion->prepare("SELECT * FROM contacto WHERE id = :id");     
        $sentencia->bindValue(':id', $lastId, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();

        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * funcion para modificar datos de contacto
     *
     * @param Array $data
     * @return Cntacto
     */
    public static function modificarContacto($data)
    {
        $sql = "UPDATE contacto 
                SET nombre = :nombre, 
                apellido = :apellido,
                mail = :mail, 
                telefono = :telefono
                WHERE id = :id";        

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $data['id']);
        $sentencia->bindValue(':nombre', $data['nombre']);
        $sentencia->bindValue(':apellido', $data['apellido']);
        $sentencia->bindValue(':mail', $data['mail']);
        $sentencia->bindValue(':telefono', $data['telefono']);
        $sentencia->bindValue(':calle', $data['calle']);
        $sentencia->execute();
        $sentencia = $conexion->prepare("SELECT * FROM contacto WHERE id = :id");     
        $sentencia->bindValue(':id', $data['id'], PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();

        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * funcion para obtener contacto por id
     *
     * @param int $idContacto
     * @return Contacto
     */
    public static function getContacto($idContacto)
    {
        $sql = "SELECT * FROM contacto WHERE id = :id";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $idContacto, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    public static function getContactos($idCliente)
    {
        $sql = "SELECT * FROM contacto WHERE cliente_id = :id";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $idCliente, PDO::PARAM_STR);
        $sentencia->execute();        
        Conexion::closeConexion();
        return $sentencia->fetchAll(PDO::FETCH_ASSOC);
    }   

    public static function getContactoPorNombre($data)
    {
        $sql = "SELECT * FROM contacto WHERE nombre = :nombre";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':nombre', $data, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    public static function asignarContactoACliente($idCliente,$idContacto){

        $sql = "UPDATE cliente SET contacto_id = :contacto_id WHERE id = :id";
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':contacto_id', $idContacto, PDO::PARAM_STR);
        $sentencia->bindValue(':id', $idCliente, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return true;
    }

    /**
     * funcion para eliminar un contacto
     *
     * @param int $idContacto
     * @return bool
     */
    public static function eliminarContacto($idContacto)
    {
        $sql = "DELETE FROM contacto WHERE id=:id";
        
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue('id', $idContacto, PDO::PARAM_STR);    
        $sentencia->execute(); 
        Conexion::closeConexion();
        return true;
    }

}