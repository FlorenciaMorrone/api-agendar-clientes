<?php namespace App\Database\Repos;

use PDO;
use App\Database\Repo;
use App\Database\Conexion;
use DateTime;

class Usuario extends Repo
{
    /**
     * funcion para obtener usuario por id
     *
     * @param int $id
     * @return Usuario
     */
    public static function getUsuario($id)
    {
        $sql = "SELECT * FROM usuario WHERE id = :id";
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $id, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * funcion para obtener usuario por mail
     *
     * @param string $mail
     * @return Usuario
     */
    public static function traerPoremail($mail)
    {
        $sql = "SELECT * FROM usuario WHERE mail = :mail";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue('mail', $mail, PDO::PARAM_STR); 

        $sentencia->execute();
        Conexion::closeConexion();   

        return $sentencia->fetch(PDO::FETCH_ASSOC);    
    }

    /**
     * funcion para crear usuario
     *
     * @param Array $data
     * @return Usuario
     */
    public static function crearUsuario($data)
    {
        $sql = "INSERT INTO usuario (mail, 
                                    password,
                                    nombre,
                                    apellido,
                                    createdAt) 
                                    VALUES (
                                        :mail, 
                                        :password,
                                        :nombre,
                                        :apellido:
                                        :createdAt)";
        
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue('mail', $data['mail'], PDO::PARAM_STR);        
        $sentencia->bindValue('password', hash('sha256', $data['password']), PDO::PARAM_STR);
        $sentencia->bindValue('nombre', $data['nombre'], PDO::PARAM_STR);   
        $sentencia->bindValue('apellido', $data['mail'], PDO::PARAM_STR);      
        $sentencia->bindValue('createdAt', new DateTime(), PDO::PARAM_STR);  
        $sentencia->execute(); 
        $lastId = $conexion->lastInsertId();  
        $sentencia = $conexion->prepare("SELECT * FROM usuario WHERE id = :id");     
        $sentencia->bindValue(':id', $lastId, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();

        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * funcion para modificar datos de Usuario
     *
     * @param Array $data
     * @return Cntacto
     */
    public static function modificarUsuario($data, $idUsuario)
    {
        $sql = "UPDATE usuario 
                SET nombre = :nombre, 
                apellido = :apellido
                updatedAt = :updatedAt
                WHERE id = :id";        

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $idUsuario);
        $sentencia->bindValue(':nombre', $data['nombre']);
        $sentencia->bindValue(':apellido', $data['apellido']);
        $sentencia->bindValue(':updatedAt', new DateTime());
        $sentencia->execute();
        $sentencia = $conexion->prepare("SELECT * FROM usuario WHERE id = :id");     
        $sentencia->bindValue(':id', $idUsuario, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();

        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }
    
}