<?php namespace App\Database\Repos;

use PDO;
use App\Database\Repo;
use App\Database\Conexion;


class Cliente extends Repo
{

    /**
     * funcion para crear un cliente
     *
     * @param Array $data
     * @return Cliente
     */
    public static function crearCliente($data)
    {
        $sql = "INSERT INTO cliente (
                                    nombre_empresa, 
                                    usuario_id) 
                                    VALUES (
                                        :nombre_empresa, 
                                        :usuario_id)";
        
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue('nombre_empresa', $data['nombre_empresa'], PDO::PARAM_STR);        
        $sentencia->bindValue('usuario_id', $data['usuario_id'], PDO::PARAM_STR);        
        $sentencia->execute(); 
        $lastId = $conexion->lastInsertId();  
        $sentencia = $conexion->prepare("SELECT * FROM cliente WHERE id = :id");     
        $sentencia->bindValue(':id', $lastId, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();

        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * funcion para modifical datos del cliente
     *
     * @param string $data
     * @param int $idCliente
     * @return Cliente
     */
    public static function modificarCliente($data,$idCliente)
    {
        $sql = "UPDATE cliente 
                SET nombre_empresa = :nombre_empresa 
                WHERE id = :id";
        
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue('nombre_empresa', $data['nombre_empresa'], PDO::PARAM_STR);  
        $sentencia->bindValue('id', $idCliente, PDO::PARAM_STR);     
        $sentencia->execute(); 
        $lastId = $conexion->lastInsertId();  
        $sentencia = $conexion->prepare("SELECT * FROM cliente WHERE id = :id");     
        $sentencia->bindValue(':id', $lastId, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();

        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * funcion para eliminar un cliente
     *
     * @param int $idCliente
     * @return bool
     */
    public static function eliminarCliente($idCliente)
    {
        $sql = "DELETE FROM cliente WHERE id=:id";
        
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue('id', $idCliente, PDO::PARAM_STR);    
        $sentencia->execute(); 
        Conexion::closeConexion();
        return true;
    }

    /**
     * funcion para obtener cliente por id
     *
     * @param int $data
     * @return Cliente
     */
    public static function getCliente($idCliente)
    {
        $sql = "SELECT * FROM cliente WHERE id = :id";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $idCliente, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * funcion para obtener un cliente por el nombre
     *
     * @param string $data
     * @return Cliente
     */
    public static function getClientePorNombre($data)
    {
        $sql = "SELECT * FROM cliente WHERE nombre_empresa = :nombre_empresa";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':nombre_empresa', $data, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }


    public static function getClientes($idUsuario)
    {
        $sql = "SELECT * FROM cliente WHERE usuario_id = :id";

        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $idUsuario, PDO::PARAM_STR);
        $sentencia->execute();        
        Conexion::closeConexion();
        return $sentencia->fetchAll(PDO::FETCH_ASSOC);
    }   

    public static function asignarSucursalACliente($idSucursal,$idCliente){

        $sql = "UPDATE cliente SET sucursal_id = :sucursal_id WHERE id = :id";
        Conexion::openConexion();
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':sucursal_id', $idSucursal, PDO::PARAM_STR);
        $sentencia->bindValue(':id', $idCliente, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return true;
    }

    
}