<?php namespace App\Controllers;

use App\Core\Abstracts\AbstractController;

class PagesController extends AbstractController
{
    public function login()
    {
        $test = [
            'first' => [
                'hola' => 'mundo'
            ],
            'second' => [
                'info' => 'empleado'
            ],
            '!qDaisB1'
        ];

        return $this->getView('login', $test);
    }
}