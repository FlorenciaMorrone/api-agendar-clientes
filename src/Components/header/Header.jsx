import React from "react";
import styled from "styled-components";
// import { Colores } from "Styles";

const HeaderComponent = styled.header``;

const Header = ({ children }) => {
  return <HeaderComponent>{children}</HeaderComponent>;
};

export default Header;
