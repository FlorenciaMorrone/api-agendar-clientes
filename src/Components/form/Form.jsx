import React, { useState, useEffect, useMemo } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const ComponentForm = styled.form``;

function Form({ children, id, width, submit }) {
  return (
    <ComponentForm onSubmit={submit} id={id} width={width}>
      {children}
    </ComponentForm>
  );
}

Form.propTypes = {
  submit: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
};

export default Form;
