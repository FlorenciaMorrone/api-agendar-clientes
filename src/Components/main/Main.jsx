import React from "react";
import styled from "styled-components";
// import { Colores } from "Styles";

const MainComponent = styled.main``;

const Main = ({ children }) => {
  return <MainComponent>{children}</MainComponent>;
};

export default Main;
