import React from "react";
import styled from "styled-components";
import { Colores } from "Styles";

const SidebarComponent = styled.aside``;

const Sidebar = ({ children }) => {
  return <SidebarComponent>{children}</SidebarComponent>;
};

export default Sidebar;
