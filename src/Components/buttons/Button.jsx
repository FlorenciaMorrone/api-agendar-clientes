import React, { useState, useEffect } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const ComponentButton = styled.button``;

function Button({ children }) {
  return <ComponentButton>{children}</ComponentButton>;
}

Button.propTypes = {};

export default Button;
