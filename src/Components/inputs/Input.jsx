import React, { useState, useEffect, useMemo } from "react";
import styled from "styled-components";
// estilos
import { Colores } from "Styles";
import PropTypes from "prop-types";

export const ComponentInput = styled.input`
  ${({ id }) =>
    id === "submit"
      ? `
    background-color:${Colores.primary};
    color:#fff;
    font-weight:bold;
    &:hover, &:active, &:focus{
      background-color:${Colores.primary};
    }
  `
      : `
     background-color:${Colores.light};
      font-weight:bold;
     &:hover, &:active, &:focus{
       background-color:${Colores.light};
     }
  `}
`;

function Input({ type, id, placeholder, value, name, setDatos, datos }) {
  const handleChange = (e) => {
    setDatos({ ...datos, [e.target.name]: e.target.value });
  };

  return (
    <ComponentInput
      id={id}
      name={name}
      type={type}
      placeholder={placeholder}
      value={value}
      onChange={handleChange}
    />
  );
}

Input.propTypes = {};

export default Input;
