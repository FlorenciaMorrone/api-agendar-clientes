import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const ComponentNotification = styled.div`
  text-align: center;
  padding: 2rem;
  border-radius: 1rem;
  margin-bottom: 1rem;

  ${({ bgColor }) =>
    bgColor === "success"
      ? `background-color: green;
        color:white;
      `
      : bgColor === "warning"
      ? `background-color: yellow;
      color:black;`
      : `background-color: red;
      color:white;`}
`;

const Notification = ({ children, bgColor }) => {
  return (
    <ComponentNotification bgColor={bgColor}>{children}</ComponentNotification>
  );
};

Notification.propTypes = {};

export default Notification;
