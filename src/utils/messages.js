import Swal from 'sweetalert2'

export const alertToast = (title, message, icon, time = 5000) => {
	Swal.fire({
		title,
		text: message,
		toast: true,
		icon: `${icon}`,
		showConfirmButton: false,
		position: 'top-right',
		timer: time,
	})
}

export const alertNormal = (title, text, icon) => {
	Swal.fire(title, text, icon)
}
