import axios from "axios";

export const cliente = async (
  url = "",
  method = "",
  datos = "",
  filters = ""
) => {
  let urlBase = "http://localhost:8000";
  const body = new FormData();
  body.append("_method", method);
  body.append("_data", JSON.stringify(datos));
  body.append("_filters", JSON.stringify(filters));

  const peticion = await axios({
    method: "POST",
    url: urlBase + url,
    data: body,
  });

  return peticion;
};
