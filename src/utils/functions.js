import { emailRegex } from "Utils/constantes";
// validar formulario
export const validacion = (data) => {
  const errores = [];
  for (let dato in data) {
    if (data[dato] === "") {
      errores.push(`${dato} es requerido`);
    } else if (dato === "user") {
      if (!emailRegex.test(data[dato])) {
        errores.push("formato de email invalido");
      }
    }
  }
  return errores;
};
