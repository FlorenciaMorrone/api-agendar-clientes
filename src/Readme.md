# Ejercicio con React JS

- El componente debe poder validar si los inputs de apellido y nombre no son vacios
- En el caso de que esten vacios debe poder mostrar un error debajo de cada input
- Se debe mostrar una leyenda del tipo de error 
- Si no existen errores el componente debe permitir mostrar el nombre y apellido ingresado
- Los estilos del componente deben cambiar (eleccion personal)
- Tiempo estimado: 60min