// styled components
import styled from "styled-components";
// crear libreria de iconos para usar valores en string
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faUser,
  faLock,
  faEye,
  faEyeSlash,
} from "@fortawesome/free-solid-svg-icons";
// añadir iconos a la libreria
library.add(faUser, faLock, faEye, faEyeSlash);

export const Colores = {
  primaryDark: "#0F1822",
  primary: "#0F3057",
  secundary: "#FCD959",
  light: "#FFFFFF",
  dark: "#111111",
  primarioLight: "#0665D4",
  green: "#C7EE74",
};

export const Alerta = styled.p`
  margin: 0;
  padding: 2rem;
  border-radius: 5px;
  font-weight: bold;
  font-size: 1.4rem;
  text-align: center;
  color: ${(props) => (props.bgYellow ? "black" : "white")};
  background-color: ${(props) =>
    props.bgRed
      ? "#9d0000bf"
      : props.bgGreen
      ? "green"
      : props.bgYellow
      ? "yellow"
      : "white"};
`;
