import React, { useState } from "react";
import { cliente } from "Utils/api";
import { validacion } from "Utils/functions";
import { Colores } from "Styles";
import styled from "styled-components";
import { Form, Input, Notification } from "Components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ComponenteLogin = styled.div`
  /* estilos del componente principal */
  position: relative; // para aplicar mascara
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100vw;
  background: ${Colores.primaryDark} url("/img/bg-login.png") no-repeat center
    center;
  background-size: cover;

  &::after {
    position: absolute;
    content: "";
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #10243a;
    opacity: 0.6;
  }

  .contenedor {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    width: min(90%, 68.5rem);
    height: 54.5rem;
    z-index: 1;
  }
  /* estilos explicito del formulario  */
  .box-input-group {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 44.5rem;
    width: min(100%, 68.5rem);
    padding: 5rem;
    border-radius: 0.5rem;
    background-color: rgba(255, 255, 255, 20%);
    #formulario {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      height: 32.3rem;

      .contenedor-input {
        position: relative;
        display: flex;
        align-items: center;
        height: 6rem;
        background-color: #fff;
        border-radius: 1.5rem;

        .icon {
          position: absolute;
          left: 2rem;
        }
        .password-icon {
          position: absolute;
          right: 2rem;
        }

        input {
          flex: 1;
          height: 6rem;
          padding: 2.5rem 6rem;
          border-radius: 1.5rem;
        }
      }
      #submit{
        height:6rem;
        border-radius:1.5rem;
      }
    }

    .recuperar-password {
      text-align: center;
      font-size: 1.4rem;
      margin-top:2rem;
    }
  }
`;
export default function Login() {
  // funcion de ver u ocultar password
  const [tipo, setTipo] = useState("password");
  //  datos
  const [datos, setDatos] = useState({
    user: "",
    password: "",
  });
  const { user, password } = datos;
  const [errores, setErrores] = useState([]);
  // funcion de ver u ocultar password
  const handlePassword = (e) => {
    e.preventDefault();

    if (tipo === "password") {
      setTipo("text");
    } else {
      setTipo("password");
    }
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const listaErrores = validacion(datos);
    setErrores(listaErrores);
    
    // validar que no alla errores
    if (listaErrores.length > 0) return;
    cliente("/auth", "login", datos)
      .then((response) => {
        if (response.status === 200) {
          location.pathname = "/home";
        }
      })
      .catch((error) => setErrores(error));

    setDatos({
      user: "",
      password: "",
    });
  };
  return (
    // componente Login
    <ComponenteLogin>
      <div className="contenedor-notificaciones">
        {errores
          ? errores.map((error) => (
              <Notification bgColor="error">{error}</Notification>
            ))
          : null}
      </div>
      <div className="contenedor">
        {/* formulario */}
        <img id="logo" src="/img/logo-hamilton.svg" alt="Logo empresa" />
        <div className="box-input-group">
          <Form id="formulario" submit={handleSubmit}>
            <div className="contenedor-input">
              <FontAwesomeIcon
                className="icon user-icon"
                icon="user"
                size="2x"
              />
              <Input
                type="text"
                value={user}
                name="user"
                setDatos={setDatos}
                datos={datos}
                placeholder="E-mail"
              />
            </div>
            <div className="contenedor-input">
              <FontAwesomeIcon
                className="icon user-icon"
                icon="lock"
                size="2x"
              />
              <Input
                type={tipo}
                value={password}
                name="password"
                setDatos={setDatos}
                datos={datos}
                placeholder="Contraseña"
              />
              <button
                className="password-icon button-no-styles"
                onClick={handlePassword}
              >
                {tipo === "password" ? (
                  <FontAwesomeIcon icon="eye" size="2x" />
                ) : (
                  <FontAwesomeIcon icon="eye-slash" size="2x" />
                )}
              </button>
            </div>
            {/* <div className="contenedor-input"> */}
              <Input id="submit" type="submit" value="Iniciar Sesión" />
            {/* </div> */}
          </Form>
          <a href="#">
            <p class="recuperar-password">He olvidado mi contraseña</p>
          </a>
        </div>
      </div>
    </ComponenteLogin>
  );
}
