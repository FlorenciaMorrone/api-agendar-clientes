import React, { useState, useEffect } from "react";
import { cliente } from "Utils/api";
import styled from "styled-components";
import { Colores } from "Styles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCloudSun,
  faSun,
  faMoon,
  faUserAlt,
  faBars,
  faCalendarAlt,
} from "@fortawesome/free-solid-svg-icons";
// componentes
import { Form, Input, Button, Header, Sidebar, Main, Cards } from "Components";

const ComponentHome = styled.div``;

export default function Home() {
  const [wSidebar, setWSidebar] = useState("0rem");

  const datos = JSON.parse(
    document.querySelector("#home").attributes.data.nodeValue
  );
  const {
    _data: {
      usuario: { primer_nombre, primer_apellido },
      general,
      ms,
    },
  } = datos;
  console.log(datos);

  // funciones
  const H = new Date().getHours();
  const Horario =
    H >= 19
      ? "Buenas Noches"
      : H >= 13 && H < 19
      ? "Buenas Tardes"
      : "Buenos Dias";

  const HIcon = H >= 19 ? faMoon : H >= 13 ? faSun : faCloudSun;

  const handleLogout = (e) => {
    e.preventDefault();

    cliente("/auth", "logout")
      .then((response) => {
        if (response.data._data.message === "See you!") {
          location.pathname = "/";
        }
      })
      .catch((error) => console.log(error));
  };

  const abriSidebar = () => {
    if (wSidebar === "0rem") {
      setWSidebar("15rem");
    } else {
      setWSidebar("0rem");
    }
  };

  // Datos de Prueba para Cards

  const microServicios = {
    0: {
      icon: "test",

      ultimoPeriodo: {
        name: "Citas Totales",
        reference: "ultimo periodo",
        value: 10,
      },
      Hoy: { name: "Citas de hoy", reference: "hoy", value: 10 },
    },
    1: {
      icon: "test2",
      ultimoPeriodo: {
        name: "Citas Totales",
        reference: "ultimo periodo",
        value: 10,
      },
      Hoy: { name: "Citas de hoy", reference: "hoy", value: 10 },
    },
    2: {
      icon: "test2",
      info: {
        0: { name: "total de citas", reference: "ultimo periodo", value: 10 },
        1: { name: "total de citas", reference: "hoy", value: 10 },
      },
    },
    3: {
      icon: "test2",
      info: {
        0: { name: "total de citas", reference: "ultimo periodo", value: 10 },
        1: { name: "total de citas", reference: "hoy", value: 10 },
      },
    },
  };

  return (
    <>
      <ComponentHome className="home">
        {/* header */}
        <Header></Header>

        {/* sidebar */}
        <Sidebar width={wSidebar}></Sidebar>
        {/* main */}
        <Main>
          <Cards></Cards>
        </Main>
      </ComponentHome>
      <button id="logout" onClick={handleLogout}>
        Logout
      </button>
    </>
  );
}
