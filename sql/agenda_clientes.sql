-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema agenda_clientes
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema agenda_clientes
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `agenda_clientes` DEFAULT CHARACTER SET utf8 ;
USE `agenda_clientes` ;

-- -----------------------------------------------------
-- Table `agenda_clientes`.`rol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda_clientes`.`rol` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agenda_clientes`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda_clientes`.`usuario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mail` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `createdAt` DATE NOT NULL,
  `updatedAt` DATE NULL,
  `rol_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_usuario_rol1_idx` (`rol_id` ASC),
  CONSTRAINT `fk_usuario_rol1`
    FOREIGN KEY (`rol_id`)
    REFERENCES `agenda_clientes`.`rol` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agenda_clientes`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda_clientes`.`cliente` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre_empresa` VARCHAR(45) NOT NULL,
  `usuario_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cliente_usuario1_idx` (`usuario_id` ASC),
  CONSTRAINT `fk_cliente_usuario1`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `agenda_clientes`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agenda_clientes`.`direccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda_clientes`.`direccion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `pais` VARCHAR(45) NOT NULL,
  `estado` VARCHAR(45) NOT NULL,
  `ciudad` VARCHAR(45) NOT NULL,
  `codigo_postal` VARCHAR(45) NOT NULL,
  `calle` VARCHAR(45) NOT NULL,
  `numero_principal` VARCHAR(45) NOT NULL,
  `numero_secundario` VARCHAR(45) NULL,
  `numero_local` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agenda_clientes`.`contacto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda_clientes`.`contacto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `mail` VARCHAR(45) CHARACTER SET 'big5' NOT NULL,
  `telefono` VARCHAR(45) NOT NULL,
  `cliente_id` INT NOT NULL,
  `direccion_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_contacto_cliente1_idx` (`cliente_id` ASC),
  INDEX `fk_contacto_direccion1_idx` (`direccion_id` ASC),
  CONSTRAINT `fk_contacto_cliente1`
    FOREIGN KEY (`cliente_id`)
    REFERENCES `agenda_clientes`.`cliente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contacto_direccion1`
    FOREIGN KEY (`direccion_id`)
    REFERENCES `agenda_clientes`.`direccion` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agenda_clientes`.`sucursal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda_clientes`.`sucursal` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `numero_sucursal` VARCHAR(45) NOT NULL,
  `cliente_id` INT NOT NULL,
  `numero_fax` VARCHAR(45) NULL,
  `direccion_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_sucursal_cliente1_idx` (`cliente_id` ASC),
  INDEX `fk_sucursal_direccion1_idx` (`direccion_id` ASC),
  CONSTRAINT `fk_sucursal_cliente1`
    FOREIGN KEY (`cliente_id`)
    REFERENCES `agenda_clientes`.`cliente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sucursal_direccion1`
    FOREIGN KEY (`direccion_id`)
    REFERENCES `agenda_clientes`.`direccion` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
