<?=
$this->layout('layouts/app', [
    'titulo' => 'Home',
    'view' => 'home'
]);
?>

<main id="home" data='<?= json_encode($data, true) ?>'></main>
